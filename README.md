# Profile Picture Rotator

Rotate your Slack profile picture by one degree everyday. 

## prerequisites

* pipenv
* python3.6
* Legacy Slack Token

## installation

* Clone this repo
* Run ```pipenv --python 3.6```
* Run ```pipenv install```
* Make a ```.env``` file, see the ```.env.example```.
* Add the following code to a crontab:

```bash
00 12 * * * cd /path/to/project && pipenv run python3.6 main.py
```
 