from PIL import Image
from datetime import date, datetime
from dotenv import load_dotenv
from pathlib import Path
import requests
import os

envPath = Path('.') / '.env'
load_dotenv(dotenv_path=envPath)

DATE_FORMAT = "%Y-%m-%d"
SIZE = 1024, 1024
API_TOKEN = os.getenv('API_TOKEN')
URL = 'https://slack.com/api/users.setPhoto?token=' + API_TOKEN + '&pretty=1'

start_date = datetime.strptime(os.getenv('START_DATE'), DATE_FORMAT).date()
current_date = date.today()
degrees = (current_date - start_date).days

profile_picture = Image.open("profile_picture.jpg")
resized_profile_picture = profile_picture.resize(SIZE, Image.LANCZOS)
rotated_profile_picture = resized_profile_picture.rotate(degrees)
rotated_profile_picture.save('rotated.jpg', 'JPEG')

files = {'image': open('rotated.jpg', 'rb')}
r = requests.post(URL, files=files)
print(r.text)

